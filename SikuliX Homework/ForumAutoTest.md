<!-- For security measures the loggin password is replaced with '????????' -->
## TC1
* Test Title: Create a new topic in forum

* Precondition:  Windows 10, Web browser Google Chrome, SekuliX, JDK11, Jython interpreter 

* Steps to reproduce:
  1. Open sikuliX
  2. Load CreateAForumTopic.sikuli and run the script

Priority: High
-----------------------
## TC2
* Test Title: Reply to un already created topic

* Precondition:  Windows 10, Web browser Google Chrome, SekuliX, JDK11, Jython interpreter 

* Steps to reproduce:
  1. Open sikuliX
  2. Load ReplyToAlreadyCreatedTopic.sikuli and run the script

Priority: High
-----------------------
<!-- For security measures the loggin password is replaced with '????????' -->