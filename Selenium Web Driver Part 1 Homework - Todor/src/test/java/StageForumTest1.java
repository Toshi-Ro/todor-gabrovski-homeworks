import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class StageForumTest1 {

    private WebDriver driver;

    @BeforeClass
    public static void ClassInit() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\tyg\\Desktop\\Telerik - QA\\Drivers\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\tyg\\Desktop\\Telerik - QA\\Drivers\\geckodriver.exe");
    }

    @Before
    public void TestInit() {
        driver = new ChromeDriver();
        driver.get("https://stage-forum.telerikacademy.com/");
    }

    @Test
    public void registeredUser_should_createNewTopic_when_useNewTopicFeature() {
        telerikLogin();

        WebElement newTopic = driver.findElement(By.id("create-topic"));
        newTopic.click();
        WebElement typeTitle = driver.findElement(By.id("reply-title"));
        typeTitle.sendKeys("WebDriver create topic test4");
        WebElement typeContent = driver.findElement(By.xpath("//textarea[@aria-label='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.']"));
        typeContent.sendKeys("Test Test Test..... give me some burgers4!!!");

        //Click button "Create topic"
        WebElement createTopic = driver.findElement(By.xpath("//button[@aria-label = 'Create Topic']//span[@class = 'd-button-label']"));
        createTopic.click();
        wait2(5000);
        //Assert
        Assert.assertTrue("Notification text not located on page", driver.getPageSource().contains("You will receive notifications because you created this topic."));

        //Click button "cancel" when you create a topic
//        WebElement cancelTopic = driver.findElement(By.xpath("//a[@class = 'cancel']"));
//        cancelTopic.click();
//        WebElement yesAbandon = driver.findElement(By.xpath("//a[contains(text(),\"Yes, abandon\")]"));
//        yesAbandon.click();

        telerikLogOut();

    }


    @Test
    public void registeredUser_should_commentToATopic_when_useCommentFeature() {
        telerikLogin();

        WebElement prepQASection = driver.findElement(By.xpath("    //div[@class='category-text-title']//span[contains(text(), \"Prep QA June 2020\")]"));
        prepQASection.click();
        wait2(3000);
        WebElement datTypes = driver.findElement(By.xpath("//div[@class='topic-title']//a[contains(text(), \"DataTypesHomework\")]"));
        datTypes.click();
        wait2(2000);
        WebElement reply = driver.findElement(By.xpath("//button[@class='btn-primary create btn btn-icon-text ember-view']"));
        reply.click();
        WebElement replyTextArea = driver.findElement(By.xpath("//textarea[@aria-label='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.']"));
        replyTextArea.sendKeys("just a selenium test reply 2");
        WebElement replyInit = driver.findElement(By.xpath("//button[@class='btn btn-icon-text btn-primary create ember-view']"));
        replyInit.click();
        wait2(3000);

        //Assert
        Assert.assertTrue("Notification text not located on page", driver.getPageSource().contains("You will see a count of new replies because you posted a reply to this topic."));

        telerikLogOut();
    }

    @After
    public void testCleаnup() {
        driver.close();
    }

    private void telerikLogin() {
        WebElement logInButton = driver.findElement(By.xpath("//span[@class = 'd-button-label']"));
        logInButton.click();
        WebElement eMail = driver.findElement(By.xpath("//input[@class='form-control ' and @placeholder = 'Email']"));
        eMail.sendKeys("tyg@abv.bg");
        WebElement password = driver.findElement(By.xpath("//input[@class='form-control ' and @placeholder = 'Password']"));
        //private password
        password.sendKeys("?????????");
        WebElement singIn = driver.findElement(By.id("next"));
        singIn.click();
    }

    private void telerikLogOut() {
        WebElement creatorAvatar = driver.findElement(By.xpath("//a[@class='icon']//Img[@class='avatar']"));
        creatorAvatar.click();
        WebElement currentUser = driver.findElement(By.xpath("//a[@class ='widget-link user-activity-link']"));
        currentUser.click();
        WebElement logOut = driver.findElement(By.xpath("//li[@class ='logout read']"));
        logOut.click();
    }

    private void wait2(long timeOutMilliseconds) {
        try {
            Thread.sleep(timeOutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
