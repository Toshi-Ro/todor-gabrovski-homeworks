package testCases;

import org.junit.Test;
import pages.GooglePage;
import pages.StageForumPage;

public class StageForumLogin extends BaseTest{
    String searchCriterion= "Telerik Academy Stage Forum";
    @Test
    public void loginInStageForum(){
        GooglePage google = new GooglePage(actions.getDriver());
        google.agreeWithConsent();
        StageForumPage stageForum = new StageForumPage(actions.getDriver());
        stageForum.openUrl();
        stageForum.loginAsRegUser();
        stageForum.assertUserIsLogged();
    }

}
