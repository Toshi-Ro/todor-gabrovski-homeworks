package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class StageForumPage extends BasePage {
    final String PAGE_URL = Utils.getConfigPropertyByKey("stageForum.url");

    public StageForumPage(WebDriver driver) {
        super(driver);
        super.setUrl(PAGE_URL);
    }

    public void openUrl() {
        Utils.getWebDriver().get(PAGE_URL);
    }

    public void loginAsRegUser() {
        actions.clickElement("forum.logButton");
        actions.typeValueInField("tyg@abv.bg", "forum.emailField");
        //private password
        actions.typeValueInField("?????????", "forum.passwordField");
        actions.clickElement("forum.next");
    }

    public void assertUserIsLogged() {
        wait2(3000);
        actions.assertElementPresent("forum.messages");
    }

    private void wait2(long timeOutMilliseconds) {
        try {
            Thread.sleep(timeOutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
