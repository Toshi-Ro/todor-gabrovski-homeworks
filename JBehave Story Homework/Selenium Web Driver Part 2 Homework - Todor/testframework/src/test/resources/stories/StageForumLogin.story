Meta:
@stageForum

Narrative:
As a Telerik student
I want login and logout in Academy forum
So i can comunicate with other students

Scenario: Stage forum login with valid credentials
Given Click forum.logButton element
And Click forum.emailField element
When Type tyg@abv.bg in forum.emailField field
When Type ???? in forum.passwordField field
And Click forum.next element
Then Wait for 3000 milliseconds
Then Click forum.icon element
Then Click forum.currentUser element
Then Click forum.logout element
