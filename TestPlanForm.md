# Test Plan
Forum’s topic creation functionality
## Testing types
* Functional testing
##  Schedule
* Test time will be done manualy by one QA and will take between 15 and 30 minutes.
## Scope of testing
### Functionalities to be tested
* Create a new topic functionality in the forum.
### Functionalities not to be tested
* Edit or delete а topic.
## Exit Criteria
* The topic is created and stored in the forum successfully.
* Times run out.