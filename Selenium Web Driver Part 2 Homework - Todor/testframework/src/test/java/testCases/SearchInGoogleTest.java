package testCases;

import org.junit.Test;
import pages.GooglePage;

public class SearchInGoogleTest extends BaseTest {
	String searchCriterion= "Telerik Academy";
	@Test
	public void simpleGoogleSearch() {
		GooglePage google = new GooglePage(actions.getDriver());
		google.agreeWithConsent();
		actions.waitForElementVisible("search.Input",10);
		actions.typeValueInField(searchCriterion, "search.Input");
		actions.waitForElementVisible("search.Button",10);
		actions.clickElement("search.Button");
		actions.waitForElementVisible("search.Result",10);
		actions.clickElement("search.Result");

		navigateToQACourseViaCard();

		actions.assertNavigatedUrl("academy.QASignUpUrl");
	}

	private void navigateToQACourseViaCard(){
		actions.clickElement("academy.AlphaAnchor");
		actions.clickElement("academy.QaGetReadyLink");
		actions.clickElement("academy.SignUpNavButton");
	}
}
