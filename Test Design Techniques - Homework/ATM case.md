ATM - Limit 400 BGN per transcation, minimal bill 10 BGN.
1. Equivalence partitioning
    < 10.00 BGN - Invalid amount || 10.00 - 400.00 BGN - Valid amount|| > 400.00 BGN - invalid amount

    ## Test 1
    Title: Withdraw less amount than minimum.
    Narrative: Try to withdraw custom amount from the menu.
    Steps to reproduce: 1.Press "Choose different amount"
                        2.Press 5 from the keyboard.
                        3.Press "Confirm"
     ## Test 2
    Title: Withdraw normal amount cash from ATM funcionality.
    Narrative: Try to withdraw amaunt from pre-design options.
    Steps to reproduce: 1.Press 200 BGN.
                        2.Enter PIN code.
                        3.Press "Confirm"
                        
                        
     ## Test 3
    Title: Withdraw more amount than maximum allowed.
    Narrative: Try to withdraw custom amount from the menu.
    Steps to reproduce: 1.Press "Choose different amount"
                        2.Press  450 from the keyboard.
                        3.Press "Confirm"

2. Boundary value analysis
  10.00 BGN - Valid amount || 400.00 BGN - Valid amount|| > 410.00 BGN - Inalid amount|| > 999.00 BGN - Inalid amount
    ## Test 3
    Title: Withdraw minimal possible amount.
    Narrative: Try to withdraw 10 BGN from the menu options.
    Steps to reproduce: 1.Press 10 BGN.
                        2.Enter PIN code.
                        3.Press "Confirm"
    ## Test 3
    Title: Withdraw maximum possible amount.
    Narrative: Try to withdraw 400 BGN from the menu options.
    Steps to reproduce: 1.Press "Choose different amount"
                        2.Press  400 from the keyboard.
                        3.Enter PIN code.
                        4.Press "Confirm"
                                            
    ## Test 5
    Title: Withdraw more than maximum allowed amount.
    Narrative: Try to withdraw custom amount from the menu.
    Steps to reproduce: 1.Press "Choose different amount"
                        2.Press  410 from the keyboard.
                        3.Press "Confirm"
                        
     ## Test 6
    Title: Withdraw more than maximum allowed amount.
    Narrative: Try to withdraw much more than normal transaction.
    Steps to reproduce: 1.Press "Choose different amount"
                        2.Press  999 from the keyboard.
                        3.Press "Confirm"

3. State Transition diagram

3.1 PIN Enter State  --> PIN is valid --> Account is accessed.
3.2 PIN Enter State  --> PIN is invalid --> PIN Enter State /up to 2 times/    
3.3 PIN Enter State /after second invalid input/ --> PIN is invalid --> Account is unavalible and the card is blocked.
 
We can use ## Test 3 for valid PIN code test and the same test with invalid PIN on step 3 for invalid PIN code test.

4. Decision Table

Card is inserted and read	                            Y	Y	Y	Y	N	N	N	N
PIN entered is correct	                              Y	Y	N	N	Y	Y	N	N
Requested amount is available in card account   	    Y	N	Y	N	Y	N	Y	N
--------------------------------------------------------------------------------------
Expected action: Funds are withdrawn	                Y	N	N	N	N	N	N	N

5. Use Case Testing

    5.1 The bank card is inserted in the slot.
    5.2 The card is read.
    5.3 User choose 100 BGN withdraw option and presses the button next to this amount.
    5.4 PIN code is requerd
    5.5 Valid PIN code is entered and validated by pressing "OK".
    5.6 User is asked for a receipt.
    5.7 "Yes" is selected for the receipt answer.
    5.8 Bank card is popped out from the slot and removed by the user.
    5.9 The cash is pooped out from the money slot and recieved by the user.
    5.10 The reciept is popped out from receipt slot and collected by the user.

